export default {
  privacy: "public",
  version: "1.0.0",
  userInterfaceStyle: "automatic",
  ios: {
    userInterfaceStyle: "light",
    bundleIdentifier: "com.panupongtipjoi.personalspendingapplication",
    buildNumber: "1.0.0",
  },
  android: {
    userInterfaceStyle: "dark",
    package: "com.panupongtipjoi.personalspendingapplication",
    versionCode: 1,
  },
};
