import { StyleSheet } from "react-native";

export const categoryStyle = StyleSheet.create({
  categoryButtonContainer: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
  button: {
    width: 130,
    padding: 20,
    backgroundColor: "#FF4A4A",
    borderWidth: 1,
    borderColor: "#FF4A4A",
    borderRadius: 24,
  },
  label: {
    color: "#fff",
    fontSize: 16,
    fontWeight: "bold",
    width: "100%",
    textAlign: "center",
  },
  categoryItemContainer: {
    marginTop: 20,
    width: "100%",
    flexDirection: "column",
    alignItems: "center",
  },
  listContainer: {
    width: "100%",
    paddingVertical: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  listTitle: {
    fontSize: 16,
    fontWeight: "600",
  },
  listActionContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  textInputContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
    marginBottom: 20,
  },
  textInput: {
    width: "85%",
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    paddingHorizontal: 20,
    zIndex: 0,
  },
});
