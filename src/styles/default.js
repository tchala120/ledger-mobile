import { StyleSheet } from "react-native";

export const defaultStyle = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    textAlign: "center",
  },
  cardContainer: {
    width: "100%",
    padding: 20,
    marginBottom: 10,
  },
  payButton: {
    padding: 20,
    width: 70,
    height: 30,
    borderRadius: 0,
    backgroundColor: "#FF4A4A",
    borderColor: "#FF4A4A",
  },
  payLabel: {
    color: "#fff",
    fontSize: 16,
    fontWeight: "bold",
    width: "100%",
    textAlign: "center",
  },
  bottomContainer: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  pickerContainer: {
    width: "100%",
    height: 150,
    backgroundColor: "#fff",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 150,
  },
});
