import { StyleSheet } from "react-native";

export const homeStyle = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  dashBoardContainer: {
    width: "50%",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 20,
  },
  dashBoardIconTypeContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    marginBottom: 10,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: 10,
  },
  resultText: {
    fontSize: 20,
    fontWeight: "600",
    color: "#00AB1B",
  },
  border: {
    borderLeftWidth: 1,
    borderColor: "#ccc",
  },
  noTransactionText: {
    marginTop: 20,
    fontSize: 16,
    fontWeight: "bold",
    textAlign: "center",
  },
  addNewTransactionButtonContainer: {
    marginVertical: 20,
    padding: 10,
    backgroundColor: "#FF4A4A",
    borderColor: "#FF4A4A",
    flexDirection: "row",
    alignItems: "center",
  },
  newTransactionLabel: {
    color: "#fff",
    fontWeight: "bold",
  },
});
