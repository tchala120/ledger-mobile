import { StyleSheet } from "react-native";

export const savingStyle = StyleSheet.create({
  container: {
    paddingTop: 10,
    paddingBottom: 0,
  },
  detailContainer: {
    flexDirection: "column",
    width: "100%",
  },
  detail: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 10,
  },
  actionContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  label: {
    fontSize: 16,
    fontWeight: "bold",
  },
  purpleValue: {
    color: "#A500CE",
  },
  blueValue: {
    color: "#1456FF",
  },
  redValue: {
    color: "#FF4A4A",
  },
});
