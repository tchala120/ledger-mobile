import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import {
  MainStackNavigator,
  CategoryStackNavigator,
  SavingStackNavigator,
  BillStackNavigator,
  SummaryStackNavigator,
} from "./Stack";

import { FontAwesome, FontAwesome5 } from "@expo/vector-icons";
import { Text } from "react-native";
import { navigationStyle } from "../styles/navigation";

const Tab = createBottomTabNavigator();

const tabScreens = [
  {
    title: "Categories",
    component: CategoryStackNavigator,
    iconName: "folder",
    type: FontAwesome,
  },
  // {
  //   title: "Saving",
  //   component: SavingStackNavigator,
  //   iconName: "inbox",
  //   type: FontAwesome,
  // },
  {
    title: "Home",
    component: MainStackNavigator,
    iconName: "home",
    type: FontAwesome,
  },
  {
    title: "Bill",
    component: BillStackNavigator,
    iconName: "receipt",
    type: FontAwesome5,
  },
  // {
  //   title: "Summary",
  //   component: SummaryStackNavigator,
  //   iconName: "pie-chart",
  //   type: FontAwesome,
  // },
];

const BottomTabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName={"Home"}
      tabBarOptions={{
        activeTintColor: "#000",
        inactiveTintColor: "#ccc",
      }}
    >
      {tabScreens.map((tab, index) => (
        <Tab.Screen
          key={index}
          name={tab.title}
          component={tab.component}
          options={() => ({
            tabBarIcon: ({ color }) => (
              <tab.type name={tab.iconName} size={24} color={color} />
            ),
            tabBarLabel: ({ color, focused }) =>
              focused && (
                <Text style={{ ...navigationStyle.label, color }}>
                  {tab.title}
                </Text>
              ),
          })}
        />
      ))}
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;
