import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Home from "../screens/Home";
import Category from "../screens/Category";
import EditCategory from "../screens/Category/EditCategory";
import Bill from "../screens/Bill";
import Saving from "../screens/Saving";
import Summary from "../screens/Summary";
import PlusButton from "../components/Button/PlusButton";
import AddCategory from "../screens/Category/AddCategory";
import CorrectButton from "../components/Button/CorrectButton";
import AddTransaction from "../screens/Home/AddTransaction";
import AddSaving from "../screens/Saving/AddSaving";
import EditSaving from "../screens/Saving/EditSaving";
import AddBill from "../screens/Bill/AddBill";
import EditBill from "../screens/Bill/EditBill";

import { Alert } from "react-native";
import { addCategory, editCategory } from "../redux/actions/category";
import { addSaving, editSaving } from "../redux/actions/saving";
import { addTransaction } from "../redux/actions/home";
import { useDispatch, useSelector } from "react-redux";
import { addBill, editBill } from "../redux/actions/bill";

const Stack = createStackNavigator();

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: "#FF4A4A",
  },
  headerTintColor: "white",
  headerBackTitle: "Back",
  headerTitleStyle: {
    fontWeight: "bold",
  },
};

const MainStackNavigator = () => {
  const { category, home } = useSelector((state) => state);
  const dispatch = useDispatch();

  const goToAddTransaction = (navigation) => {
    if (
      category.category.incomeCategory.length === 0 &&
      category.category.expenseCategory.length === 0
    )
      return Alert.alert("Please add category first.");
    else return navigation.navigate("AddTransaction");
  };

  const onCreateNew = (navigation) => {
    dispatch(addTransaction(home));
    navigation.goBack();
  };

  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen
        name="Home"
        component={Home}
        options={({ navigation }) => ({
          headerRight: () => (
            <PlusButton onPress={() => goToAddTransaction(navigation)} />
          ),
        })}
      />
      <Stack.Screen
        name="AddTransaction"
        component={AddTransaction}
        options={({ navigation }) => ({
          title: "Add Transaction",
          headerRight:
            home.title && home.value && home.date
              ? () => <CorrectButton onPress={() => onCreateNew(navigation)} />
              : null,
        })}
      />
    </Stack.Navigator>
  );
};

const CategoryStackNavigator = () => {
  const { category } = useSelector((state) => state);
  const dispatch = useDispatch();

  const onCreateNew = (navigation) => {
    dispatch(addCategory(category));
    navigation.goBack();
  };

  const onUpdate = (navigation) => {
    dispatch(editCategory(category));
    navigation.goBack();
  };

  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen
        name="Categories"
        component={Category}
        options={({ navigation }) => ({
          headerRight: () => (
            <PlusButton onPress={() => navigation.navigate("AddCategory")} />
          ),
        })}
      />
      <Stack.Screen
        name="AddCategory"
        component={AddCategory}
        options={({ navigation }) => ({
          title: "Add Category",
          headerRight: category.newCategory
            ? () => <CorrectButton onPress={() => onCreateNew(navigation)} />
            : null,
        })}
      />
      <Stack.Screen
        name="EditCategory"
        component={EditCategory}
        options={({ navigation }) => ({
          title: "Edit Category",
          headerRight: category.editCategory.title
            ? () => <CorrectButton onPress={() => onUpdate(navigation)} />
            : null,
        })}
      />
    </Stack.Navigator>
  );
};

const SavingStackNavigator = () => {
  const { saving } = useSelector((state) => state);
  const dispatch = useDispatch();

  const onCreateNew = (navigation) => {
    dispatch(
      addSaving({
        title: saving.title,
        goal: saving.goal,
      })
    );
    navigation.goBack();
  };

  const onUpdate = (navigation) => {
    dispatch(editSaving(saving));
    navigation.goBack();
  };

  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen
        name="Saving"
        component={Saving}
        options={({ navigation }) => ({
          headerRight: () => (
            <PlusButton onPress={() => navigation.navigate("AddSaving")} />
          ),
        })}
      />
      <Stack.Screen
        name="AddSaving"
        component={AddSaving}
        options={({ navigation }) => ({
          title: "Add Saving",
          headerRight:
            saving.title && saving.goal
              ? () => <CorrectButton onPress={() => onCreateNew(navigation)} />
              : null,
        })}
      />
      <Stack.Screen
        name="EditSaving"
        component={EditSaving}
        options={({ navigation }) => ({
          title: "Edit Saving",
          headerRight:
            saving.saving.title && saving.saving.goal
              ? () => <CorrectButton onPress={() => onUpdate(navigation)} />
              : null,
        })}
      />
    </Stack.Navigator>
  );
};

const BillStackNavigator = () => {
  const { bill } = useSelector((state) => state);
  const dispatch = useDispatch();

  console.log("Bill", bill);

  const onCreateNew = (navigation) => {
    dispatch(
      addBill({
        name: bill.name,
        value: bill.value,
        date: bill.date,
      })
    );
    navigation.goBack();
  };

  const onUpdate = (navigation) => {
    dispatch(editBill(bill));
    navigation.goBack();
  };

  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen
        name="Bill"
        component={Bill}
        options={({ navigation }) => ({
          headerRight: () => (
            <PlusButton onPress={() => navigation.navigate("AddBill")} />
          ),
        })}
      />
      <Stack.Screen
        name="AddBill"
        component={AddBill}
        options={({ navigation }) => ({
          title: "Add Bill",
          headerRight:
            bill.name && bill.value && bill.date
              ? () => <CorrectButton onPress={() => onCreateNew(navigation)} />
              : null,
        })}
      />
      <Stack.Screen
        name="EditBill"
        component={EditBill}
        options={({ navigation }) => ({
          title: "Edit Bill",
          headerRight:
            bill.bill.name && bill.bill.value && bill.bill.date
              ? () => <CorrectButton onPress={() => onUpdate(navigation)} />
              : null,
        })}
      />
    </Stack.Navigator>
  );
};

const SummaryStackNavigator = () => {
  return (
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Summary" component={Summary} />
    </Stack.Navigator>
  );
};

export {
  MainStackNavigator,
  CategoryStackNavigator,
  SavingStackNavigator,
  BillStackNavigator,
  SummaryStackNavigator,
};
