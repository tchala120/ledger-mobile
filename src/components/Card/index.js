import React from "react";
import PropTypes from "prop-types";
import { Card } from "native-base";
import { defaultStyle } from "../../styles/default";
import ListItem from "../Category/ListItem";
import { Divider } from "react-native-elements";
import { savingStyle } from "../../styles/saving";
import { View } from "react-native";

export default function CardTask({
  navigation,
  children,
  data,
  goTo,
  onEdit,
  onDelete,
}) {
  return (
    <Card style={defaultStyle.cardContainer}>
      <ListItem
        data={data}
        navigation={navigation}
        containerStyle={savingStyle.container}
        goTo={goTo}
        onEdit={onEdit}
        onDelete={onDelete}
      />
      <Divider style={{ backgroundColor: "#000", marginVertical: 10 }} />
      <View style={savingStyle.detailContainer}>{children}</View>
    </Card>
  );
}

CardTask.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
  children: PropTypes.node.isRequired,
  data: PropTypes.object.isRequired,
  goTo: PropTypes.string.isRequired,
  onEdit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};
