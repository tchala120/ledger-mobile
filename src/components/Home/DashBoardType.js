import { Feather } from "@expo/vector-icons";
import React from "react";
import { View, Text } from "react-native";
import { homeStyle } from "../../styles/home";

export default function DashBoardType({
  iconName,
  title,
  result,
  type,
  containerStyle,
}) {
  return (
    <View style={{ ...homeStyle.dashBoardContainer, ...containerStyle }}>
      <View style={homeStyle.dashBoardIconTypeContainer}>
        <Feather name={iconName} size={18} color="black" />
        <Text style={homeStyle.title}>{title}</Text>
      </View>
      <Text
        style={
          type === "INCOME"
            ? homeStyle.resultText
            : { ...homeStyle.resultText, color: "#FF4A4A" }
        }
      >
        {type === "EXPENSE" && result > 0 && "-"}
        {result}
      </Text>
    </View>
  );
}
