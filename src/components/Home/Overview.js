import { Card } from "native-base";
import React from "react";
import { View, Text } from "react-native";
import { Divider } from "react-native-elements";
import { defaultStyle } from "../../styles/default";
import { homeStyle } from "../../styles/home";
import { calBalance, getFinalBalance } from "../../utils/transaction";
import ListItem from "../List/ListItem";
import DashBoardType from "./DashBoardType";

export default function Overview({ data, transaction }) {
  return (
    <Card style={defaultStyle.cardContainer}>
      <ListItem
        leftTitle="Balance"
        rightTitle={getFinalBalance(transaction)}
        valueColor="#00AB1B"
      />
      <View
        style={{
          width: "100%",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
        }}
      >
        <DashBoardType
          iconName="download"
          title="Income"
          result={calBalance(transaction, "INCOME")}
          type="INCOME"
        />
        <DashBoardType
          iconName="upload"
          title="Expense"
          result={calBalance(transaction, "EXPENSE")}
          type="EXPENSE"
          containerStyle={homeStyle.border}
        />
      </View>
      <Divider style={{ marginVertical: 10 }} />
      <ListItem
        leftTitle="Saving"
        rightTitle={calBalance(transaction, "SAVING")}
        valueColor="#1456FF"
        isLastItem
      />
    </Card>
  );
}
