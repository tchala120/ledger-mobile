import React from "react";
import PropTypes from "prop-types";
import { Card } from "native-base";
import dayjs from "dayjs";
import { View, Text } from "react-native";
import { defaultStyle } from "../../styles/default";
import ListItem from "../List/ListItem";
import { homeStyle } from "../../styles/home";
import { calSpendingInMonth } from "../../utils/transaction";

export default function MonthList({ date, transactions }) {
  const getColorOfTransaction = (type) => {
    switch (type) {
      case "INCOME":
        return "#00AB1B";
      case "EXPENSE":
        return "#FF4A4A";
      case "SAVING":
        return "#1456FF";
      default:
        return;
    }
  };

  const getColorOfBalanceInMonth = (balance) => {
    if (balance < 0) return "#FF4A4A";
    else return "#00AB1B";
  };

  return (
    <Card style={defaultStyle.cardContainer}>
      <ListItem
        leftTitle={dayjs(date).format("MMM YYYY")}
        rightTitle={calSpendingInMonth(transactions)}
        valueColor={getColorOfBalanceInMonth(calSpendingInMonth(transactions))}
      />
      {transactions.length > 0 ? (
        transactions.map((transaction, index) => (
          <ListItem
            key={index}
            leftTitle={transaction.title}
            rightTitle={transaction.value}
            valueColor={getColorOfTransaction(transaction.type)}
            type={transaction.type}
            isLastItem
          />
        ))
      ) : (
        <Text style={homeStyle.noTransactionText}>
          Have no Transaction in this month
        </Text>
      )}
    </Card>
  );
}

MonthList.propTypes = {
  date: PropTypes.string.isRequired,
  transactions: PropTypes.array,
};
