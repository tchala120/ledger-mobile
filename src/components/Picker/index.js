import React from "react";
import { View, Text, Modal, Picker } from "react-native";
import { defaultStyle } from "../../styles/default";
import { firstUpperCase } from "../../utils/string";

export default function PickerModal({
  category,
  categories,
  handleChange,
  name,
  value,
}) {
  return (
    <Modal transparent animationType="slide">
      <View style={defaultStyle.bottomContainer}>
        <View style={defaultStyle.pickerContainer}>
          <Picker
            selectedValue={category[name]}
            style={{ height: 50, width: "80%", marginVertical: 20 }}
            onValueChange={(value) => handleChange(value, name)}
          >
            {categories.map((catagoryType, index) => {
              return (
                <Picker.Item
                  label={value ? catagoryType[value] : catagoryType.label}
                  value={
                    value
                      ? catagoryType[value].toLowerCase()
                      : catagoryType.value
                  }
                  key={index}
                />
              );
            })}
          </Picker>
        </View>
      </View>
    </Modal>
  );
}
