import React from "react";
import PropTypes from "prop-types";
import { TouchableOpacity } from "react-native";
import { FontAwesome } from "@expo/vector-icons";

export default function BackButton({ onPress }) {
  return (
    <TouchableOpacity onPress={onPress}>
      <FontAwesome
        name="chevron-left"
        size={24}
        color="#fff"
        style={{ marginRight: 10 }}
      />
    </TouchableOpacity>
  );
}

BackButton.propTypes = {
  onPress: PropTypes.func.isRequired,
};
