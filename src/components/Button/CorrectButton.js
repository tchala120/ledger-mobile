import React from "react";
import PropTypes from "prop-types";
import { TouchableOpacity } from "react-native";
import { AntDesign, FontAwesome } from "@expo/vector-icons";

export default function CorrectButton({ onPress }) {
  return (
    <TouchableOpacity onPress={onPress}>
      <FontAwesome
        name="check"
        size={24}
        color="#fff"
        style={{ marginRight: 10 }}
      />
    </TouchableOpacity>
  );
}

CorrectButton.propTypes = {
  onPress: PropTypes.func.isRequired,
};
