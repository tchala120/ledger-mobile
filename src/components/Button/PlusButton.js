import React from "react";
import PropTypes from "prop-types";
import { TouchableOpacity } from "react-native";
import { AntDesign } from "@expo/vector-icons";

export default function PlusButton({ onPress }) {
  return (
    <TouchableOpacity onPress={onPress}>
      <AntDesign
        name="pluscircle"
        size={24}
        color="#fff"
        style={{ marginRight: 10 }}
      />
    </TouchableOpacity>
  );
}

PlusButton.propTypes = {
  onPress: PropTypes.func.isRequired,
};
