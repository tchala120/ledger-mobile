import React from "react";
import PropTypes from "prop-types";
import { Button } from "native-base";
import { Text } from "react-native";
import { categoryStyle } from "../../styles/category";

export default function CategoryButton({ title, onPress, isPressed }) {
  return (
    <Button
      onPress={onPress}
      style={
        isPressed
          ? categoryStyle.button
          : { ...categoryStyle.button, backgroundColor: "#fff" }
      }
    >
      <Text
        style={
          isPressed
            ? categoryStyle.label
            : { ...categoryStyle.label, color: "#FF4A4A" }
        }
      >
        {title}
      </Text>
    </Button>
  );
}

CategoryButton.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  isPressed: PropTypes.bool,
};
