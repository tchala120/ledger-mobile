import React from "react";
import PropTypes from "prop-types";
import { FontAwesome5 } from "@expo/vector-icons";
import { View, Text, TouchableOpacity } from "react-native";
import { categoryStyle } from "../../styles/category";
import { Divider } from "react-native-elements";

export default function ListItem({
  leftTitle,
  rightTitle,
  containerStyle,
  valueColor,
  isLastItem,
  type,
}) {
  return (
    <>
      <View style={[categoryStyle.listContainer, containerStyle]}>
        <Text style={categoryStyle.listTitle}>{leftTitle}</Text>
        <Text style={{ ...categoryStyle.listTitle, color: valueColor }}>
          {type === "EXPENSE" ? `-${rightTitle}` : rightTitle}
        </Text>
      </View>
      {!isLastItem && <Divider style={{ marginVertical: 10 }} />}
    </>
  );
}

ListItem.propTypes = {
  leftTitle: PropTypes.string.isRequired,
  rightTitle: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  containerStyle: PropTypes.object,
  valueColor: PropTypes.string,
  isLastItem: PropTypes.bool,
};
