import { Card } from "native-base";
import React from "react";
import { View, Text } from "react-native";
import { defaultStyle } from "../../styles/default";
import ListItem from "../List/ListItem";
import dayjs from "dayjs";

export default function SummaryCard({ navigation }) {
  return (
    <Card style={defaultStyle.cardContainer}>
      <ListItem
        leftTitle="Balance"
        rightTitle="50000"
        valueColor="#00AB1B"
        navigation={navigation}
      />
      <ListItem
        leftTitle="Filter Month"
        rightTitle={dayjs().format("MM / YYYY")}
        navigation={navigation}
        valueColor="#000"
        isLastItem
      />
    </Card>
  );
}
