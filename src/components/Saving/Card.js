import React from "react";
import PropTypes from "prop-types";
import { FontAwesome } from "@expo/vector-icons";
import { View, Text } from "react-native";
import { savingStyle } from "../../styles/saving";
import CardTask from "../Card";

export default function SavingCard({
  data,
  navigation,
  goTo,
  onEdit,
  onDelete,
}) {
  return (
    <CardTask
      navigation={navigation}
      data={data}
      goTo={goTo}
      onEdit={onEdit}
      onDelete={onDelete}
    >
      <View style={savingStyle.detail}>
        <Text style={savingStyle.label}>Goal</Text>
        <Text style={[savingStyle.label, savingStyle.purpleValue]}>
          {data.goal}
        </Text>
      </View>
      <View style={savingStyle.detail}>
        <Text style={savingStyle.label}>Saved</Text>
        <View style={savingStyle.actionContainer}>
          <FontAwesome
            name="plus"
            size={24}
            color="#000"
            style={{ marginRight: 10 }}
          />
          <Text style={[savingStyle.label, savingStyle.blueValue]}>
            {data.saved}
          </Text>
        </View>
      </View>
      <View style={savingStyle.detail}>
        <Text style={savingStyle.label}>Reminding</Text>
        <Text style={[savingStyle.label, savingStyle.purpleValue]}>
          {data.remaining}
        </Text>
      </View>
    </CardTask>
  );
}

SavingCard.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
  data: PropTypes.object,
  goTo: PropTypes.string.isRequired,
  onEdit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};
