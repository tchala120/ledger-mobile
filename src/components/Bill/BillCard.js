import { FontAwesome } from "@expo/vector-icons";
import React from "react";
import { View, Text, Alert } from "react-native";
import { savingStyle } from "../../styles/saving";
import CardTask from "../Card";
import dayjs from "dayjs";
import { Button } from "native-base";
import { defaultStyle } from "../../styles/default";
import { useDispatch } from "react-redux";
import { addTransaction } from "../../redux/actions/home";
import { deleteBill } from "../../redux/actions/bill";

export default function BillCard({ data, goTo, onEdit, onDelete, navigation }) {
  const dispatch = useDispatch();

  const onPayComplete = () => {
    dispatch(
      addTransaction({
        title: data.name,
        value: data.value,
        type: "EXPENSE",
        date: dayjs(data.date).format("YYYY-MM"),
      })
    );
    dispatch(deleteBill(data));
  };

  return (
    <CardTask
      data={data}
      goTo={goTo}
      onDelete={onDelete}
      onEdit={onEdit}
      navigation={navigation}
    >
      <View style={savingStyle.detail}>
        <Text style={savingStyle.label}>Total</Text>
        <Text style={[savingStyle.label, savingStyle.redValue]}>
          {data.value}
        </Text>
      </View>
      <View style={savingStyle.detail}>
        <View style={savingStyle.actionContainer}>
          <FontAwesome
            name="calendar-check-o"
            size={24}
            color="black"
            style={{ marginRight: 10 }}
          />
          <Text style={[savingStyle.label]}>
            {dayjs(data.date).format("DD MMM YYYY")}
          </Text>
        </View>
        <Button style={defaultStyle.payButton} onPress={() => onPayComplete()}>
          <Text style={defaultStyle.payLabel}>Pay</Text>
        </Button>
      </View>
    </CardTask>
  );
}
