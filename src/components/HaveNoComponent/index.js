import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { homeStyle } from "../../styles/home";
import { firstUpperCase } from "../../utils/string";
import PlusButton from "../Button/PlusButton";

export default function HaveNoComponent({ onPress, type, textStyle }) {
  return (
    <View style={homeStyle.container}>
      <Text style={homeStyle.noTransactionText}>Have no {type}</Text>
      <TouchableOpacity onPress={onPress}>
        <View style={homeStyle.addNewTransactionButtonContainer}>
          <PlusButton onPress={onPress} />
          <Text style={[homeStyle.newTransactionLabel, textStyle]}>
            {firstUpperCase(type)}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}
