import React from "react";
import PropTypes from "prop-types";
import { FontAwesome5 } from "@expo/vector-icons";
import { View, Text, TouchableOpacity, Alert } from "react-native";
import { categoryStyle } from "../../styles/category";
import { useDispatch } from "react-redux";

export default function ListItem({
  data,
  navigation,
  containerStyle,
  goTo,
  onEdit,
  onDelete,
}) {
  const dispatch = useDispatch();

  const goToEditScreen = () => {
    dispatch(onEdit(data));
    navigation.navigate(goTo);
  };

  const confirmDeleteAlert = () => {
    Alert.alert(
      `Delete ${data.title}`,
      "Are you sure to delete",
      [
        {
          text: "Cancel",
          style: "cancel",
        },
        { text: "OK", onPress: () => dispatch(onDelete(data)) },
      ],
      { cancelable: false }
    );
  };

  return (
    <View style={[categoryStyle.listContainer, containerStyle]}>
      <Text style={categoryStyle.listTitle}>{data.title}</Text>
      <View style={categoryStyle.listActionContainer}>
        <TouchableOpacity style={{ marginRight: 10 }} onPress={goToEditScreen}>
          <FontAwesome5 name="edit" size={24} color="black" />
        </TouchableOpacity>
        <TouchableOpacity onPress={confirmDeleteAlert}>
          <FontAwesome5 name="trash" size={24} color="black" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

ListItem.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
  containerStyle: PropTypes.object,
  data: PropTypes.object,
};
