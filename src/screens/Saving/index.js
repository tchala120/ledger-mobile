import React from "react";
import PropTypes from "prop-types";
import Layout from "../../containers/Layout";
import SavingCard from "../../components/Saving/Card";
import { useSelector } from "react-redux";
import HaveNoComponent from "../../components/HaveNoComponent";
import { deleteSaving, setSaving } from "../../redux/actions/saving";

const SavingScreen = ({ navigation }) => {
  const { saving } = useSelector((state) => state);

  const goToAddNewSaving = () => {
    navigation.navigate("AddSaving");
  };

  return (
    <Layout backgroundColor="#D2D2D2">
      {saving.savings.length > 0 ? (
        saving.savings.map((saving) => {
          return (
            <SavingCard
              key={saving.id}
              navigation={navigation}
              data={saving}
              goTo="EditSaving"
              onEdit={setSaving}
              onDelete={deleteSaving}
            />
          );
        })
      ) : (
        <HaveNoComponent onPress={goToAddNewSaving} type="saving" />
      )}
    </Layout>
  );
};

export default SavingScreen;

SavingScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
