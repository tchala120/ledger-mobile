import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  TextInput,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  Picker,
  Modal,
} from "react-native";
import Layout from "../../containers/Layout";
import { Button, Card } from "native-base";
import { categoryStyle } from "../../styles/category";
import { FontAwesome } from "@expo/vector-icons";
import { defaultStyle } from "../../styles/default";
import PickerModal from "../../components/Picker";
import { useDispatch, useSelector } from "react-redux";
import { onChangeCategoryInput } from "../../redux/actions/category";
import { onChangeInputSaving } from "../../redux/actions/saving";

const categories = [
  {
    label: "Income",
    value: "income",
  },
  {
    label: "Expense",
    value: "expense",
  },
];

export default function AddSaving() {
  const dispatch = useDispatch();
  const { saving } = useSelector((state) => state);

  const handleChange = (value, name) => {
    dispatch(
      onChangeInputSaving({
        ...saving,
        [name]: value,
      })
    );
  };

  return (
    <Layout backgroundColor="#D2D2D2">
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <KeyboardAvoidingView
          style={{ width: "100%", flex: 1 }}
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          keyboardVerticalOffset={Platform.OS === "ios" ? 40 : 20}
        >
          <Card style={defaultStyle.cardContainer}>
            <View
              style={{
                ...categoryStyle.textInputContainer,
                marginBottom: 20,
              }}
            >
              <FontAwesome name="inbox" size={24} />
              <TextInput
                style={categoryStyle.textInput}
                placeholder={"AirPods Max"}
                onChangeText={(value) => handleChange(value, "title")}
                value={saving.title}
              />
            </View>
            <View
              style={{ ...categoryStyle.textInputContainer, marginBottom: 0 }}
            >
              <View></View>
              <TextInput
                style={categoryStyle.textInput}
                placeholder={"19900"}
                keyboardType="number-pad"
                onChangeText={(value) => handleChange(value, "goal")}
                value={saving.goal > 0 ? saving.goal : ""}
              />
            </View>
          </Card>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    </Layout>
  );
}
