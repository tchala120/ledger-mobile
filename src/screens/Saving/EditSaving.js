import React from "react";
import {
  View,
  TextInput,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
} from "react-native";
import Layout from "../../containers/Layout";
import { Card } from "native-base";
import { categoryStyle } from "../../styles/category";
import { FontAwesome } from "@expo/vector-icons";
import { defaultStyle } from "../../styles/default";
import { useDispatch, useSelector } from "react-redux";
import { onChangeEditInputSaving } from "../../redux/actions/saving";

export default function EditSaving() {
  const dispatch = useDispatch();
  const { saving } = useSelector((state) => state);

  const handleChange = (value, name) => {
    dispatch(
      onChangeEditInputSaving({
        ...saving.saving,
        [name]: value,
      })
    );
  };

  return (
    <Layout backgroundColor="#D2D2D2">
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <KeyboardAvoidingView
          style={{ width: "100%", flex: 1 }}
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          keyboardVerticalOffset={Platform.OS === "ios" ? 40 : 20}
        >
          <Card style={defaultStyle.cardContainer}>
            <View
              style={{
                ...categoryStyle.textInputContainer,
                marginBottom: 20,
              }}
            >
              <FontAwesome name="inbox" size={24} />
              <TextInput
                style={categoryStyle.textInput}
                placeholder={"AirPods Max"}
                onChangeText={(value) => handleChange(value, "title")}
                value={saving.saving.title}
              />
            </View>
            <View
              style={{ ...categoryStyle.textInputContainer, marginBottom: 0 }}
            >
              <View></View>
              <TextInput
                style={categoryStyle.textInput}
                placeholder={"19900"}
                keyboardType="number-pad"
                onChangeText={(value) => handleChange(value, "goal")}
                value={saving.saving.goal > 0 ? saving.saving.goal : ""}
              />
            </View>
          </Card>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    </Layout>
  );
}
