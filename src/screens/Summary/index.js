import React from "react";
import PropTypes from "prop-types";
import { Text } from "react-native";
import Layout from "../../containers/Layout";
import SummaryCard from "../../components/Summary/Card";
import Overview from "../../components/Summary/Overview";

const SummaryScreen = ({ navigation }) => {
  return (
    <Layout>
      <SummaryCard navigation={navigation} />
      <Overview />
      <SummaryCard navigation={navigation} />
    </Layout>
  );
};

export default SummaryScreen;

SummaryScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
