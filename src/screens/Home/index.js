import React from "react";
import PropTypes from "prop-types";
import { Alert } from "react-native";
import Layout from "../../containers/Layout";
import Overview from "../../components/Home/Overview";
import MonthList from "../../components/Home/MonthList";
import { useSelector } from "react-redux";
import HaveNoComponent from "../../components/HaveNoComponent";

const HomeScreen = ({ navigation }) => {
  const { category, home } = useSelector((state) => state);

  const goToAddNewTransaction = () => {
    if (
      category.category.incomeCategory.length === 0 &&
      category.category.expenseCategory.length === 0
    )
      return Alert.alert("Please add category first.");
    else return navigation.navigate("AddTransaction");
  };

  const renderTransactionByMonth = () => {
    const transactionMonth = Object.keys(home.transaction || {});

    if (transactionMonth.length === 0)
      return (
        <HaveNoComponent onPress={goToAddNewTransaction} type="transaction" />
      );
    else {
      return transactionMonth.map((month, index) => (
        <MonthList
          date={month}
          key={index}
          transactions={home.transaction[month]}
        />
      ));
    }
  };

  return (
    <Layout>
      <Overview data={home.overview} transaction={home.transaction} />
      {renderTransactionByMonth()}
    </Layout>
  );
};

export default HomeScreen;

HomeScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
