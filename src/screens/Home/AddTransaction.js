import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  TextInput,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
} from "react-native";
import Layout from "../../containers/Layout";
import { Button, Card } from "native-base";
import { categoryStyle } from "../../styles/category";
import { FontAwesome } from "@expo/vector-icons";
import { defaultStyle } from "../../styles/default";
import PickerModal from "../../components/Picker";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import dayjs from "dayjs";
import { useDispatch, useSelector } from "react-redux";
import { firstUpperCase } from "../../utils/string";
import { onChangeTransactionInput } from "../../redux/actions/home";

export default function AddTransaction({ navigaiton }) {
  const { category } = useSelector((state) => state);
  const dispatch = useDispatch();

  const getAllCategories = () => {
    if (
      category.category.incomeCategory.length > 0 &&
      category.category.expenseCategory.length > 0
    )
      return [
        {
          label: "Income",
          value: "income",
        },
        {
          label: "Expense",
          value: "expense",
        },
      ];
    else if (
      category.category.incomeCategory.length > 0 &&
      category.category.expenseCategory.length === 0
    )
      return [
        {
          label: "Income",
          value: "income",
        },
      ];
    else if (
      category.category.incomeCategory.length > 0 &&
      category.category.expenseCategory.length === 0
    )
      return [
        {
          label: "Expense",
          value: "expense",
        },
      ];
  };

  const categories = getAllCategories();

  const [subCategories, setSubCategories] = useState(
    category.category.incomeCategory
  );
  const [isPickerCategory, setPickerCategory] = useState(false);
  const [isPickerCategoryDetail, setPickerCategoryDetail] = useState(false);
  const [isDatePicker, setDatePicker] = useState(false);
  const [transaction, setTransaction] = useState({
    type: category.type,
    title: category.category.incomeCategory[0].title,
    value: "",
    date: dayjs(),
  });

  const handleChange = (value, name) => {
    setTransaction({
      ...transaction,
      [name]: value,
    });
    dispatch(
      onChangeTransactionInput({
        ...transaction,
        [name]: value,
      })
    );
    if (name === "type") {
      setSubCategories(
        value === "income"
          ? category.category.incomeCategory
          : category.category.expenseCategory
      );
      setTransaction({
        ...transaction,
        [name]: value,
        title:
          value === "income"
            ? category.category.incomeCategory[0].title
            : category.category.expenseCategory[0].title,
      });
      dispatch(
        onChangeTransactionInput({
          ...transaction,
          [name]: value,
          title:
            value === "income"
              ? category.category.incomeCategory[0].title
              : category.category.expenseCategory[0].title,
        })
      );
    }
    onTogglePickerCategory(false);
    onTogglePickerCategoryDetail(false);
    onToggleDatePicker(false);
  };

  const onTogglePickerCategory = (isShow) => {
    setPickerCategory(isShow);
  };

  const onTogglePickerCategoryDetail = (isShow) => {
    setPickerCategoryDetail(isShow);
  };

  const onToggleDatePicker = (isShow) => {
    setDatePicker(isShow);
  };

  return (
    <Layout backgroundColor="#D2D2D2">
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <KeyboardAvoidingView
          style={{ width: "100%", flex: 1 }}
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          keyboardVerticalOffset={Platform.OS === "ios" ? 40 : 20}
        >
          <Card style={defaultStyle.cardContainer}>
            <View style={categoryStyle.textInputContainer}>
              <FontAwesome name="folder" size={24} />
              <Button
                style={{
                  ...categoryStyle.textInput,
                  backgroundColor: "transparent",
                  borderRadius: 0,
                }}
                onPress={onTogglePickerCategory}
                disabled={categories.length <= 1}
              >
                <Text style={{ color: "#000" }}>
                  {firstUpperCase(transaction.type)}
                </Text>
              </Button>
            </View>
            <View
              style={{
                ...categoryStyle.textInputContainer,
              }}
            >
              <View></View>
              <Button
                style={{
                  ...categoryStyle.textInput,
                  backgroundColor: "transparent",
                  borderRadius: 0,
                }}
                onPress={onTogglePickerCategoryDetail}
              >
                <Text style={{ color: "#000" }}>
                  {firstUpperCase(transaction.title)}
                </Text>
              </Button>
            </View>
            <View style={categoryStyle.textInputContainer}>
              <FontAwesome name="money" size={24} />
              <TextInput
                style={categoryStyle.textInput}
                keyboardType="number-pad"
                onChangeText={(value) => handleChange(value, "value")}
                value={transaction.value}
              />
            </View>
            <View style={categoryStyle.textInputContainer}>
              <FontAwesome name="calendar-check-o" size={24} />
              <Button
                style={{
                  ...categoryStyle.textInput,
                  backgroundColor: "transparent",
                  borderRadius: 0,
                }}
                onPress={onToggleDatePicker}
              >
                <Text style={{ color: "#000" }}>
                  {dayjs(transaction.date).format("DD / MM / YYYY")}
                </Text>
              </Button>
            </View>
          </Card>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
      {isPickerCategory && (
        <PickerModal
          categories={categories}
          category={transaction}
          handleChange={handleChange}
          name="type"
        />
      )}
      {isPickerCategoryDetail && (
        <PickerModal
          categories={subCategories}
          category={transaction}
          handleChange={handleChange}
          name="title"
          value={"title"}
        />
      )}
      {isDatePicker && (
        <DateTimePickerModal
          isVisible={Boolean(isDatePicker)}
          maximumDate={dayjs().toDate()}
          mode="date"
          onConfirm={(value) => handleChange(value, "date")}
          onCancel={() => setDatePicker(false)}
        />
      )}
    </Layout>
  );
}

AddTransaction.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
