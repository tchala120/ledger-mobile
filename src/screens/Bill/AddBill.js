import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  TextInput,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
} from "react-native";
import Layout from "../../containers/Layout";
import { Button, Card } from "native-base";
import { categoryStyle } from "../../styles/category";
import { FontAwesome, FontAwesome5 } from "@expo/vector-icons";
import { defaultStyle } from "../../styles/default";
import PickerModal from "../../components/Picker";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import dayjs from "dayjs";
import { useDispatch, useSelector } from "react-redux";
import { firstUpperCase } from "../../utils/string";
import { onChangeBillInput } from "../../redux/actions/bill";

export default function AddBill({ navigaiton }) {
  const { bill } = useSelector((state) => state);
  const dispatch = useDispatch();

  const [isDatePicker, setDatePicker] = useState(false);
  const [billData, setBillData] = useState({
    name: "",
    value: 0,
    date: dayjs(),
  });

  const handleChange = (value, name) => {
    setBillData({
      ...billData,
      [name]: value,
    });
    dispatch(
      onChangeBillInput({
        ...billData,
        [name]: value,
      })
    );
    onToggleDatePicker(false);
  };

  const onToggleDatePicker = (isShow) => {
    setDatePicker(isShow);
  };

  return (
    <Layout backgroundColor="#D2D2D2">
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <KeyboardAvoidingView
          style={{ width: "100%", flex: 1 }}
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          keyboardVerticalOffset={Platform.OS === "ios" ? 40 : 20}
        >
          <Card style={defaultStyle.cardContainer}>
            <View style={categoryStyle.textInputContainer}>
              <FontAwesome5 name="receipt" size={24} />
              <TextInput
                style={categoryStyle.textInput}
                placeholder="Internet Bill"
                onChangeText={(value) => handleChange(value, "name")}
                value={billData.name}
              />
            </View>
            <View style={categoryStyle.textInputContainer}>
              <FontAwesome name="money" size={24} />
              <TextInput
                style={categoryStyle.textInput}
                keyboardType="number-pad"
                placeholder="1290"
                onChangeText={(value) => handleChange(value, "value")}
                value={billData.value > 0 ? billData.value : ""}
              />
            </View>
            <View style={categoryStyle.textInputContainer}>
              <FontAwesome name="calendar-check-o" size={24} />
              <Button
                style={{
                  ...categoryStyle.textInput,
                  backgroundColor: "transparent",
                  borderRadius: 0,
                }}
                onPress={onToggleDatePicker}
              >
                <Text style={{ color: "#000" }}>
                  {dayjs(billData.date).format("DD / MM / YYYY")}
                </Text>
              </Button>
            </View>
          </Card>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
      {isDatePicker && (
        <DateTimePickerModal
          isVisible={Boolean(isDatePicker)}
          mode="date"
          onConfirm={(value) => handleChange(value, "date")}
          onCancel={() => setDatePicker(false)}
        />
      )}
    </Layout>
  );
}

AddBill.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
