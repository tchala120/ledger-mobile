import React from "react";
import PropTypes from "prop-types";
import { Text } from "react-native";
import Layout from "../../containers/Layout";
import BillCard from "../../components/Bill/BillCard";
import { useSelector } from "react-redux";
import HaveNoComponent from "../../components/HaveNoComponent";
import { deleteBill, setBill } from "../../redux/actions/bill";

const BillScreen = ({ navigation }) => {
  const { bill } = useSelector((state) => state);

  const goToAddNewBill = () => {
    navigation.navigate("AddBill");
  };

  return (
    <Layout backgroundColor="#D2D2D2">
      {bill.bills.length > 0 ? (
        bill.bills.map((bill) => (
          <BillCard
            key={bill.id}
            data={{
              ...bill,
              title: bill.name,
            }}
            navigation={navigation}
            goTo="EditBill"
            onEdit={setBill}
            onDelete={deleteBill}
          />
        ))
      ) : (
        <HaveNoComponent onPress={goToAddNewBill} type="bill" />
      )}
    </Layout>
  );
};

export default BillScreen;

BillScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
