import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  TextInput,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  Picker,
  Modal,
} from "react-native";
import Layout from "../../containers/Layout";
import { Button, Card } from "native-base";
import { categoryStyle } from "../../styles/category";
import { FontAwesome } from "@expo/vector-icons";
import { defaultStyle } from "../../styles/default";
import PickerModal from "../../components/Picker";
import { useDispatch, useSelector } from "react-redux";
import { onChangeCategoryInput } from "../../redux/actions/category";
import { firstUpperCase } from "../../utils/string";

const categories = [
  {
    label: "Income",
    value: "income",
  },
  {
    label: "Expense",
    value: "expense",
  },
];

export default function AddCategory({ navigaiton }) {
  const dispatch = useDispatch();
  const { category } = useSelector((state) => state);

  const [isPickerShow, setPicker] = useState(false);

  const handleChange = (value, name) => {
    dispatch(
      onChangeCategoryInput({
        ...category,
        [name]: value,
        label: firstUpperCase(value),
      })
    );
    if (name === "type") onTogglePicker();
  };

  const onTogglePicker = () => {
    setPicker(!isPickerShow);
  };

  return (
    <Layout backgroundColor="#D2D2D2">
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <KeyboardAvoidingView
          style={{ width: "100%", flex: 1 }}
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          keyboardVerticalOffset={Platform.OS === "ios" ? 40 : 20}
        >
          <Card style={defaultStyle.cardContainer}>
            <View
              style={{
                ...categoryStyle.textInputContainer,
                marginBottom: 20,
              }}
            >
              <FontAwesome name="folder" size={24} />
              <Button
                style={{
                  ...categoryStyle.textInput,
                  backgroundColor: "transparent",
                  borderRadius: 0,
                }}
                onPress={onTogglePicker}
              >
                <Text style={{ color: "#000" }}>
                  {firstUpperCase(category.type)}
                </Text>
              </Button>
            </View>
            <View
              style={{ ...categoryStyle.textInputContainer, marginBottom: 0 }}
            >
              <View></View>
              <TextInput
                style={categoryStyle.textInput}
                placeholder={
                  category.type === "income" ? "Salary" : "Internet Bill"
                }
                onChangeText={(value) => handleChange(value, "newCategory")}
                value={category.newCategory}
              />
            </View>
          </Card>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
      {isPickerShow && (
        <PickerModal
          categories={categories}
          category={category}
          handleChange={handleChange}
          name={"type"}
        />
      )}
    </Layout>
  );
}

AddCategory.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
