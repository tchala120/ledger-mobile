import React, { useState } from "react";
import PropTypes from "prop-types";
import { Text, TouchableOpacity, View } from "react-native";
import Layout from "../../containers/Layout";
import CategoryButton from "../../components/Button/CategoryButton";
import { categoryStyle } from "../../styles/category";
import { FontAwesome5 } from "@expo/vector-icons";
import ListItem from "../../components/Category/ListItem";
import { useSelector } from "react-redux";
import { homeStyle } from "../../styles/home";
import {
  setCategory,
  deleteCategory,
} from "../../redux/actions/category";

const Category = ({ navigation }) => {
  const [category, setCategoryType] = useState("income");

  const categoryStore = useSelector((state) => state.category);

  return (
    <Layout>
      <View style={categoryStyle.categoryButtonContainer}>
        <CategoryButton
          title="Income"
          onPress={() => setCategoryType("income")}
          isPressed={category === "income"}
        />
        <CategoryButton
          title="Expense"
          onPress={() => setCategoryType("expense")}
          isPressed={category === "expense"}
        />
      </View>
      <View style={categoryStyle.categoryItemContainer}>
        {category === "income" ? (
          categoryStore.category.incomeCategory.length > 0 ? (
            categoryStore.category.incomeCategory.map((category, index) => {
              return (
                <ListItem
                  key={index}
                  navigation={navigation}
                  data={{
                    ...category,
                    type: "income",
                  }}
                  goTo={"EditCategory"}
                  onEdit={setCategory}
                  onDelete={deleteCategory}
                />
              );
            })
          ) : (
            <View>
              <Text style={homeStyle.noTransactionText}>
                Have no income category
              </Text>
            </View>
          )
        ) : categoryStore.category.expenseCategory.length > 0 ? (
          categoryStore.category.expenseCategory.map((category, index) => {
            return (
              <ListItem
                key={index}
                navigation={navigation}
                data={{
                  ...category,
                  type: "expense",
                }}
                goTo={"EditCategory"}
                onEdit={setCategory}
                onDelete={deleteCategory}
              />
            );
          })
        ) : (
          <View>
            <Text style={homeStyle.noTransactionText}>
              Have no expense category
            </Text>
          </View>
        )}
      </View>
    </Layout>
  );
};

export default Category;

Category.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
