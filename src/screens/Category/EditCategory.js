import React from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  TextInput,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
} from "react-native";
import Layout from "../../containers/Layout";
import { Button, Card } from "native-base";
import { categoryStyle } from "../../styles/category";
import { FontAwesome } from "@expo/vector-icons";
import { defaultStyle } from "../../styles/default";
import { useDispatch, useSelector } from "react-redux";
import { onChangeEditCategoryInput } from "../../redux/actions/category";

export default function EditCategory({ navigaiton }) {
  const dispatch = useDispatch();
  const { category } = useSelector((state) => state);

  const handleChange = (value, name) => {
    dispatch(
      onChangeEditCategoryInput({
        ...category,
        [name]: value,
      })
    );
  };

  return (
    <Layout backgroundColor="#D2D2D2">
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <KeyboardAvoidingView
          style={{ width: "100%", flex: 1 }}
          behavior={Platform.OS === "ios" ? "padding" : "height"}
          keyboardVerticalOffset={Platform.OS === "ios" ? 40 : 20}
        >
          <Card style={defaultStyle.cardContainer}>
            <View
              style={{
                ...categoryStyle.textInputContainer,
                marginBottom: 20,
              }}
            >
              <FontAwesome name="folder" size={24} />
              <Button
                style={{
                  ...categoryStyle.textInput,
                  backgroundColor: "transparent",
                  borderRadius: 0,
                  borderColor: "rgba(0, 0, 0, 0.5)",
                }}
                disabled
              >
                <Text style={{ color: "#000", opacity: 0.5 }}>
                  {category.type.charAt(0).toUpperCase() +
                    category.type.slice(1)}
                </Text>
              </Button>
            </View>
            <View
              style={{ ...categoryStyle.textInputContainer, marginBottom: 0 }}
            >
              <View></View>
              <TextInput
                style={categoryStyle.textInput}
                placeholder={
                  category.type === "income" ? "Salary" : "Internet Bill"
                }
                onChangeText={(value) => handleChange(value, "title")}
                value={category.editCategory.title}
              />
            </View>
          </Card>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    </Layout>
  );
}

EditCategory.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
