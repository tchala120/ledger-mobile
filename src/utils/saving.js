export const addNewSaving = (savings, newSaving) => {
  return [
    ...savings,
    {
      ...newSaving,
      id: savings.length,
      remaining: 0,
      saved: 0,
    },
  ];
};

export const editSaving = (savings, editSaving) => {
  return savings.map((saving) => {
    if (saving.id == editSaving.saving.id)
      return {
        ...saving,
        ...editSaving.saving,
      };
    else return { ...saving };
  });
};

export const deleteSaving = (savings, deleteCategory) => {
  return savings.filter((category) => category.id != deleteCategory.id);
};
