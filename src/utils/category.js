export const addNewCategory = (category, newCategory, type) => {
  switch (type) {
    case "income":
      return {
        ...category,
        incomeCategory: [
          ...category.incomeCategory,
          {
            id: category.incomeCategory.length,
            title: newCategory,
          },
        ],
      };
    case "expense":
      return {
        ...category,
        expenseCategory: [
          ...category.expenseCategory,
          {
            id: category.expenseCategory.length,
            title: newCategory,
          },
        ],
      };
    default:
      return;
  }
};

export const editCategory = (category, editCategory, type, id) => {
  switch (type) {
    case "income":
      return {
        ...category,
        incomeCategory: findCategoryAndUpdate(
          category.incomeCategory,
          id,
          editCategory
        ),
      };
    case "expense":
      return {
        ...category,
        expenseCategory: findCategoryAndUpdate(
          category.expenseCategory,
          id,
          editCategory
        ),
      };
    default:
      return;
  }
};

export const findCategoryAndUpdate = (categories, id, updateCategory) => {
  return categories.map((category, index) => {
    if (id == index) {
      return {
        ...category,
        title: updateCategory,
      };
    } else {
      return {
        ...category,
      };
    }
  });
};

export const deleteCategory = (category, deleteCategory) => {
  switch (deleteCategory.type) {
    case "income":
      return {
        ...category,
        incomeCategory: filterNotDeleteItem(
          category.incomeCategory,
          deleteCategory.id
        ),
      };
    case "expense":
      return {
        ...category,
        expenseCategory: filterNotDeleteItem(
          category.expenseCategory,
          deleteCategory.id
        ),
      };
    default:
      return;
  }
};

export const filterNotDeleteItem = (categories, id) => {
  return categories.filter((category) => category.id != id);
};
