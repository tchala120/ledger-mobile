import dayjs from "dayjs";

export const isSameMonth = (d1, d2) => {
  return dayjs(d1).isSame(dayjs(d2), "M");
};

export const addNewTransaction = (transaction = {}, newTransaction) => {
  const transactionDate = Object.keys(transaction);
  if (transactionDate.length > 0) {
    for (let value in transaction) {
      if (isSameMonth(value, newTransaction.date)) {
        return {
          ...transaction,
          [value]: [
            {
              title: newTransaction.title,
              value: parseFloat(newTransaction.value),
              type: newTransaction.type,
            },
            ...transaction[value],
          ],
        };
      } else {
        return {
          ...transaction,
          [dayjs(newTransaction.date).format("YYYY-MM")]: [
            {
              title: newTransaction.title,
              value: parseFloat(newTransaction.value),
              type: newTransaction.type,
            },
          ],
        };
      }
    }
  } else {
    return {
      ...transaction,
      [dayjs(newTransaction.date).format("YYYY-MM")]: [
        {
          title: newTransaction.title,
          value: parseFloat(newTransaction.value),
          type: newTransaction.type,
        },
      ],
    };
  }
};

export const caseCalculate = (type) => {
  switch (type) {
    default:
    case "INCOME":
      return "+";
    case "EXPENSE":
    case "SAVING":
      return "-";
  }
};

export const calSpendingInMonth = (transactions = []) => {
  let balanceInMonth = 0;
  transactions.map((transaction) => {
    balanceInMonth = eval(
      `${balanceInMonth} ${caseCalculate(transaction.type)} ${
        transaction.value
      }`
    );
  });
  return balanceInMonth;
};

export const calBalance = (transaction = {}, type) => {
  const transactionsDate = Object.keys(transaction);

  let balance = 0;

  transactionsDate.map((date) => {
    transaction[date].map((spend) => {
      if (spend.type === type) {
        balance = eval(
          `${balance} ${caseCalculate(spend.type)} ${spend.value}`
        );
      }
    });
  });

  return type === "SAVING" ? Math.abs(balance) : balance;
};

export const getFinalBalance = (transaction = {}) => {
  return calBalance(transaction, "INCOME") + calBalance(transaction, "EXPENSE");
};
