export const addNewBill = (bills, newBill) => {
  return [
    ...bills,
    {
      ...newBill,
      id: bills.length,
      remaining: 0,
      saved: 0,
    },
  ];
};

export const editBill = (bills, editBill) => {
  return bills.map((bill) => {
    if (bill.id == editBill.bill.id)
      return {
        ...bill,
        ...editBill.bill,
      };
    else return { ...bill };
  });
};

export const deleteBill = (bills, deletedBill) => {
  return bills.filter((bill) => bill.id != deletedBill.id);
};
