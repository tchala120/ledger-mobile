import dayjs from "dayjs";
import { addNewTransaction } from "../../utils/transaction";
import {
  SET_TRANSACTION,
  ADD_TRANSACTION,
  CHANGE_TRANSACTION,
} from "../actions/home";
import { RESET_TYPE } from "../type";

const initialState = {
  overview: {
    balance: 0.0,
    income: 0.0,
    expense: 0.0,
    saving: 0.0,
  },
  transaction: {
    [dayjs().format("YYYY-MM")]: [],
    [dayjs().subtract(1, "M").format("YYYY-MM")]: [],
    [dayjs().subtract(2, "M").format("YYYY-MM")]: [],
    [dayjs().subtract(3, "M").format("YYYY-MM")]: [],
    [dayjs().subtract(4, "M").format("YYYY-MM")]: [],
    [dayjs().subtract(5, "M").format("YYYY-MM")]: [],
  },
  title: "",
  value: 0,
  type: "",
  date: "",
};
export default home = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_TRANSACTION:
      return {
        ...state,
        ...payload,
      };
    case ADD_TRANSACTION:
      return {
        ...state,
        transaction: addNewTransaction(state.transaction, {
          title: payload.title,
          value: payload.value,
          type: payload.type.toUpperCase(),
          date: dayjs(payload.date).format("YYYY-MM"),
        }),
      };
    case CHANGE_TRANSACTION:
      return {
        ...state,
        ...payload,
      };
    case RESET_TYPE:
      return initialState;
    default:
      return state;
  }
};
