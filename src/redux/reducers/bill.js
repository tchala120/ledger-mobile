import dayjs from "dayjs";
import { addNewBill, deleteBill, editBill } from "../../utils/bill";
import {
  ADD_BILL,
  CHANGE_BILL,
  CHANGE_EDIT_BILL,
  DELETE_BILL,
  EDIT_BILL,
  SET_BILL,
  SET_BILLS,
} from "../actions/bill";
import { RESET_TYPE } from "../type";

const initialState = {
  bills: [],
  bill: {},
  name: "",
  value: 0,
  date: dayjs(),
};
export default bill = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_BILLS:
      return {
        ...state,
        ...payload,
      };
    case SET_BILL:
      return {
        ...state,
        bill: payload,
      };
    case ADD_BILL:
      return {
        ...state,
        bills: addNewBill(state.bills, payload),
        title: "",
        total: 0,
      };
    case EDIT_BILL:
      return {
        ...state,
        bills: editBill(state.bills, payload),
      };
    case DELETE_BILL:
      return {
        ...state,
        bills: deleteBill(state.bills, payload),
      };
    case CHANGE_BILL:
      return {
        ...state,
        name: payload.name,
        value: payload.value,
        date: payload.date,
      };
    case CHANGE_EDIT_BILL:
      return {
        ...state,
        bill: {
          ...state.bill,
          ...payload,
        },
      };
    case RESET_TYPE:
      return initialState;
    default:
      return state;
  }
};
