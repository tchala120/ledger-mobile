import { combineReducers } from "redux";
import bill from "./bill";
import category from "./category";
import home from "./home";
import saving from "./saving";
import summary from "./summary";

const rootReducer = combineReducers({
  bill,
  home,
  category,
  saving,
  //   summary,
});

export default rootReducer;
