import {
  addNewCategory,
  deleteCategory,
  editCategory,
} from "../../utils/category";
import {
  SET_CATEGORY,
  ADD_CATEGORY,
  CHANGE_CATEGORY,
  EDIT_CATEGORY,
  DELELTE_CATEGORY,
  SET_CATEGORIES,
  CHANGE_EDIT_CATEGORY,
} from "../actions/category";
import { RESET_TYPE } from "../type";

const initialState = {
  category: {
    incomeCategory: [],
    expenseCategory: [],
  },
  newCategory: "",
  type: "income",
  editCategory: {},
};
export default category = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_CATEGORIES:
      return {
        ...state,
        ...payload,
      };
    case SET_CATEGORY:
      return {
        ...state,
        type: payload.type,
        editCategory: {
          id: payload.id,
          type: payload.type,
          title: payload.title,
        },
      };
    case ADD_CATEGORY:
      return {
        ...state,
        category: addNewCategory(
          state.category,
          payload.newCategory,
          payload.type
        ),
        newCategory: "",
        type: "income",
      };
    case CHANGE_CATEGORY:
      return {
        ...state,
        newCategory: payload.newCategory,
        type: payload.type,
      };
    case CHANGE_EDIT_CATEGORY:
      return {
        ...state,
        editCategory: {
          ...state.editCategory,
          title: payload.title,
        },
      };
    case EDIT_CATEGORY:
      return {
        ...state,
        category: editCategory(
          state.category,
          payload.editCategory.title,
          payload.editCategory.type,
          payload.editCategory.id
        ),
        type: "income",
      };
    case DELELTE_CATEGORY:
      return {
        ...state,
        category: deleteCategory(state.category, payload),
      };
    case RESET_TYPE:
      return initialState;
    default:
      return state;
  }
};
