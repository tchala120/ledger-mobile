import dayjs from "dayjs";
import { addNewSaving, deleteSaving, editSaving } from "../../utils/saving";
import {
  ADD_SAVING,
  CHANGE_EDIT_SAVING,
  CHANGE_SAVING,
  DELETE_SAVING,
  EDIT_SAVING,
  SET_SAVING,
  SET_SAVINGS,
} from "../actions/saving";
import { RESET_TYPE } from "../type";

const initialState = {
  savings: [],
  saving: {},
  title: "",
  goal: 0,
};
export default saving = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_SAVINGS:
      return {
        ...state,
        ...payload,
      };
    case SET_SAVING:
      return {
        ...state,
        saving: payload,
      };
    case ADD_SAVING:
      return {
        ...state,
        savings: addNewSaving(state.savings, payload),
        title: "",
        goal: 0,
      };
    case EDIT_SAVING:
      return {
        ...state,
        savings: editSaving(state.savings, payload),
      };
    case DELETE_SAVING:
      return {
        ...state,
        savings: deleteSaving(state.savings, payload),
      };
    case CHANGE_SAVING:
      return {
        ...state,
        title: payload.title,
        goal: payload.goal,
      };
    case CHANGE_EDIT_SAVING:
      return {
        ...state,
        saving: {
          ...state.saving,
          ...payload,
        },
      };
    case RESET_TYPE:
      return initialState;
    default:
      return state;
  }
};
