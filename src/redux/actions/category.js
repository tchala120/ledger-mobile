export const SET_CATEGORIES = "SET_CATEGORIES";
export const SET_CATEGORY = "SET_CATEGORY";
export const ADD_CATEGORY = "ADD_CATEGORY";
export const CHANGE_CATEGORY = "CHANGE_CATEGORY";
export const CHANGE_EDIT_CATEGORY = "CHANGE_EDIT_CATEGORY";
export const EDIT_CATEGORY = "EDIT_CATEGORY";
export const DELELTE_CATEGORY = "DELELTE_CATEGORY";

export const setCategories = (payload) => (dispatch) => {
  return dispatch({
    type: SET_CATEGORIES,
    payload,
  });
};

export const setCategory = (payload) => (dispatch) => {
  return dispatch({
    type: SET_CATEGORY,
    payload,
  });
};

export const addCategory = (payload) => (dispatch) => {
  return dispatch({
    type: ADD_CATEGORY,
    payload,
  });
};

export const onChangeCategoryInput = (payload) => (dispatch) => {
  return dispatch({
    type: CHANGE_CATEGORY,
    payload,
  });
};

export const onChangeEditCategoryInput = (payload) => (dispatch) => {
  return dispatch({
    type: CHANGE_EDIT_CATEGORY,
    payload,
  });
};

export const editCategory = (payload) => (dispatch) => {
  return dispatch({
    type: EDIT_CATEGORY,
    payload,
  });
};

export const deleteCategory = (payload) => (dispatch) => {
  return dispatch({
    type: DELELTE_CATEGORY,
    payload,
  });
};
