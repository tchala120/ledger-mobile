export const SET_BILLS = "SET_BILLS";
export const SET_BILL = "SET_BILL";
export const ADD_BILL = "ADD_BILL";
export const EDIT_BILL = "EDIT_BILL";
export const DELETE_BILL = "DELETE_BILL";
export const CHANGE_BILL = "CHANGE_BILL";
export const CHANGE_EDIT_BILL = "CHANGE_EDIT_BILL";

export const setBills = (payload) => (dispatch) => {
  return dispatch({
    type: SET_BILLS,
    payload,
  });
};

export const setBill = (payload) => (dispatch) => {
  return dispatch({
    type: SET_BILL,
    payload,
  });
};

export const addBill = (payload) => (dispatch) => {
  return dispatch({
    type: ADD_BILL,
    payload,
  });
};

export const editBill = (payload) => (dispatch) => {
  return dispatch({
    type: EDIT_BILL,
    payload,
  });
};

export const deleteBill = (payload) => (dispatch) => {
  return dispatch({
    type: DELETE_BILL,
    payload,
  });
};

export const onChangeBillInput = (payload) => (dispatch) => {
  return dispatch({
    type: CHANGE_BILL,
    payload,
  });
};

export const onChangeEditBillInput = (payload) => (dispatch) => {
  return dispatch({
    type: CHANGE_EDIT_BILL,
    payload,
  });
};
