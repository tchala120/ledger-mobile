export const SET_TRANSACTION = "SET_TRANSACTION";
export const ADD_TRANSACTION = "ADD_TRANSACTION";
export const CHANGE_TRANSACTION = "CHANGE_TRANSACTION";

export const setTransaction = (payload) => (dispatch) => {
  return dispatch({
    type: SET_TRANSACTION,
    payload,
  });
};

export const addTransaction = (payload) => (dispatch) => {
  return dispatch({
    type: ADD_TRANSACTION,
    payload,
  });
};

export const onChangeTransactionInput = (payload) => (dispatch) => {
  return dispatch({
    type: CHANGE_TRANSACTION,
    payload,
  });
};
