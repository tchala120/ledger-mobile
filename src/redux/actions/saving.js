export const SET_SAVINGS = "SET_SAVINGS";
export const SET_SAVING = "SET_SAVING";
export const ADD_SAVING = "ADD_SAVING";
export const EDIT_SAVING = "EDIT_SAVING";
export const DELETE_SAVING = "DELETE_SAVING";
export const CHANGE_SAVING = "CHANGE_SAVING";
export const CHANGE_EDIT_SAVING = "CHANGE_EDIT_SAVING";

export const setSavings = (payload) => (dispatch) => {
  return dispatch({
    type: SET_SAVINGS,
    payload,
  });
};

export const setSaving = (payload) => (dispatch) => {
  return dispatch({
    type: SET_SAVING,
    payload,
  });
};

export const addSaving = (payload) => (dispatch) => {
  return dispatch({
    type: ADD_SAVING,
    payload,
  });
};

export const editSaving = (payload) => (dispatch) => {
  return dispatch({
    type: EDIT_SAVING,
    payload,
  });
};

export const deleteSaving = (payload) => (dispatch) => {
  return dispatch({
    type: DELETE_SAVING,
    payload,
  });
};

export const onChangeInputSaving = (payload) => (dispatch) => {
  return dispatch({
    type: CHANGE_SAVING,
    payload,
  });
};

export const onChangeEditInputSaving = (payload) => (dispatch) => {
  return dispatch({
    type: CHANGE_EDIT_SAVING,
    payload,
  });
};
