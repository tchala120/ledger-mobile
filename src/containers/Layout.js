import React from "react";
import PropTypes from "prop-types";
import { ScrollView, View } from "react-native";
import { defaultStyle } from "../styles/default";

export default function Layout({ children, backgroundColor, isNoPaddLeft }) {
  return (
    <ScrollView
      style={{
        flex: 1,
        width: "100%",
        paddingVertical: 15,
        paddingHorizontal: !isNoPaddLeft ? 15 : 0,
        backgroundColor,
      }}
    >
      <View style={{ ...defaultStyle.container }}>{children}</View>
    </ScrollView>
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
  backgroundColor: PropTypes.string,
  isPaddLeft: PropTypes.bool,
};
